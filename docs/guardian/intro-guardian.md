---
sidebar_position: 7
---

# Introduction à Guardian

``guardian`` est un package inclus dans PinguinOS qui sert à sécuriser votre machine.

:::note

_guardian ne protège pas réellement vote machine._

:::

## Installer `guardian`

Pour installer guardian il suffit d'exécuter la commande suivante : 

```
/pg-install guardian
```

## Utiliser `guardian`

Pour utiliser scanmap la syntaxe est la suivante :

```
/scanmap <action>
```