---
sidebar_position: 1
---

# PinguinOS

Un jeu où la sécurité informatique devient une aventure, et où les joueurs deviennent les maîtres de leur destin numérique. Êtes-vous prêt à relever le défi et à devenir le champion de PinguinOS ? Plongez dans le monde virtuel de la cybersécurité dès aujourd'hui !

![Banner](./img/banner.png)

