---
sidebar_position: 4
---

# Commandes système

## Informations sur le système 

La commande pour trouver des informations sur le système est la suivante : 

```
/os-config
```

## Installation de package

Pour installer des package via commande il suffit de faire la commande :

```
/pg-install <@package>
```

## Liste des fichiers 

Les fichiers en extension `.pn`, sont des fichiers qui contiennent une clé de wallet. Pour lister vos fichiers il suffit de faire la commande :

```
/ls
```

## Mise à jour de session

Pour mettre à jour votre session il suffit d'exécuter la commande : 

```
/session-update
```

:::warning

Si votre session n'est pas maintenue à jour elle peut être sujette à des failles de sécurité.

:::

## Status de packages

Vous pouvez vérifier le status de vos package pour savoir s'ils sont installés, actif ou non. Pour se faire il suffit d'entrer la commande :

```
/status <@package>
```

Les packages sont inclus dans PinguinOS.