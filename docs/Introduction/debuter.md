---
sidebar_position: 3
---

# Débuter sur PinguinOS

## S'enregistrer puis démarrer une machine

Pour commencer appuyez une fois sur lancer une machine puis cela vous enregistrera. Ensuite relancez une machine.

![LancerMachine](../img/launch.png)

## Créer sa session

:::info

Afin de pouvoir démarrer il faut impérativement créer sa session.

:::

Ensuite, il suffit de créer sa session avec la commande suivante : 

```
/session-create
```