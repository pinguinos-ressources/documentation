---
sidebar_position: 8
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Utilisation de xortia

## Prérequis

Dans un premier temps vérifiez que vous avez bien installé le package sinon revenez sur vos pas à cette [étape](https://pinguinos-ressources.gitlab.io/documentation/docs/xortia/intro-xortia).


## Récupérer à partir d'une adresse

Pour récupérer des informations à partir d'une adresse il suffit d'utiliser l'action `grab -f`. La commande est donc la suivante :

```
/xortia grab -f <address>
```

Vous obtienderez alors des informations sur les machines environnantes.


