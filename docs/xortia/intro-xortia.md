---
sidebar_position: 7
---

# Introduction à xortia

``xortia`` est un package inclus dans PinguinOS qui sert à récupérer des informations .

:::note

_xortia ne récupère pas réellement des informations privées._

:::

## Installer `xortia`

Pour installer xortia il suffit d'exécuter la commande suivante : 

```
/pg-install xortia
```

## Utiliser `xortia`

Pour utiliser scanmap la syntaxe est la suivante :

```
/xortia <action> <target>
```