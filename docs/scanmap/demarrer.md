---
sidebar_position: 7
---

# Démarrer avec scanmap

``scanmap`` est un package inclus dans PinguinOS qui sert à détecter les autres machines actives.

:::note

_scanmap ne scan pas réellement un réseau._

:::

## Installer `scanmap`

Pour installer scanmap il suffit d'exécuter la commande suivante : 

```
/pg-install scanmap
```

## Utiliser `scanmap`

Pour utiliser scanmap la syntaxe est la suivante :

```
/scanmap <action>
```