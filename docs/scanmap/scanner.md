---
sidebar_position: 8
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Scanner avec scanmap

## Prérequis

Dans un premier temps vérifiez que vous avez bien installé le package sinon revenez sur vos pas à cette [étape](https://pinguinos-ressources.gitlab.io/documentation/docs/scanmap/demarrer/).

<Tabs>
  <TabItem value="run" label="Activer" default>
    Pour utiliser scanmap il faut l'activer sur votre machine. Pour se faire il faut exécuter la commande suivante : 

    ```
    /scanmap run
    ```
  </TabItem>
  <TabItem value="disarm" label="Désactiver">
    Pour désactiver scanmap il faut exécuter la commande suivante : 

    ```
    /scanmap disarm
    ```
  </TabItem>
</Tabs>

## Lancer un scan

Pour lancer un scan il suffit d'utiliser l'action `scan-all`. La commande est donc la suivante :

```
/scanmap -all
```

Vous obtienderez alors des informations sur les machines environnantes.


