---
slug: 14-12-2023-patch-note
title: Patch note 14-12-2023
authors: [zentrax]
tags: [Patch Note, Documentation]
date: 2023-12-14T18:00
---


# Patch note 14-12-2023 : 

## Le patch note entre dans le blog !

Afin de vous informer des mises à jour sur PinguinOS ou sur la documentation, le patch note sera publié dans le blog !

## Nouvelles pages dans la documentation 

[Débuter](https://pinguinos-ressources.gitlab.io/documentation/docs/Introduction/debuter) et [Commandes systèmes](https://pinguinos-ressources.gitlab.io/documentation/docs/Introduction/commandes) vous attendent ! Ces deux pages favoriseront votre approche du jeu si vous êtes débutant.

