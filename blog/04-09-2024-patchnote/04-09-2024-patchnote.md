---
slug: 04-09-2024-patchnote
title: Patch note 04-09-2024
authors: [zentrax]
tags: [Patch Note, Documentation]
date: 2023-12-19T18:00
---

# Patch note 04-09-2024 : 

## Documentation

- Catégorie pour xortia
- Catégorie pour guardian

## Blog

_Il ne se passe rien par ici..._

## PinguinOS

- Ajout de l'action 'use' sur safehorse
- Ajoute de l'action 'delete' sur guardian