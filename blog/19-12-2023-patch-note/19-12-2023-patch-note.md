---
slug: 19-12-2023-patch-note
title: Patch note 19-12-2023
authors: [zentrax]
tags: [Patch Note, Documentation]
date: 2023-12-19T18:00
---

# Patch note 19-12-2023 : 

## Documentation

- Catégorie pour les badges et leur utilité.
- Catégorie pour le package scanmap.

## Blog

- Fix des dates de posts sur le blog.

## PinguinOS

_Il ne se passe rien par ici..._